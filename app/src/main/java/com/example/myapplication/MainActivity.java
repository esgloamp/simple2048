package com.example.myapplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity implements View.OnClickListener {
    public TextView tvCurrentScore;// 展示分数
    public int score = 0; // 当前分数

    public TextView tvBestScore;// 展示最高分
    private int bestScore;//历史最高成绩

    private Button btnRestart; // 重新开始按钮

    private static MainActivity mainActivity = null;

    public MainActivity() {
        mainActivity = this;
    }

    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    /**
     * 初始化
     */
    public void init() {
        tvBestScore = findViewById(R.id.tv_best_score);
        tvCurrentScore = findViewById(R.id.tv_current_score);
        btnRestart = findViewById(R.id.btn_restart);
        btnRestart.setOnClickListener(this); // 添加监听事件
        BestScore bestScore = new BestScore(this);
        this.bestScore = bestScore.getBestScore();
        tvBestScore.setText("历史最高：" + this.bestScore);

    }

    /**
     * 重新开始按钮监听时间
     * @param v
     */
    @Override
    public void onClick(View v) {
        score = 0;
        showScore();
        GameView.getGameView().startGame();
    }

    /**
     * 展示分数
     */
    public void showScore() {
        tvCurrentScore.setText("当前分数：" + score + "  ");
    }

    /**
     * 增加分数，如果超过了历史最高分数，则同步更新历史最高分数
     * @param s
     */
    public void addScore(int s) {
        score += s;
        showScore();
        if (score > bestScore) {
            bestScore = score;
            BestScore bs = new BestScore(this);
            bs.setBestScore(bestScore);
            tvBestScore.setTextSize(20); // 放大
            tvBestScore.setText("历史最高：" + bestScore);
        }
    }

    /**
     * 双击返回键退出
     */
    private long firstExitTime = 0;
    @SuppressLint("WrongConstant")
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - firstExitTime) > 2000) { // 和第一次按下事件相差2秒以上
                Toast.makeText(this, "再按一次退出", 1000).show();
                firstExitTime = System.currentTimeMillis(); // 记录当前按下的时间
            } else {
                finish();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
