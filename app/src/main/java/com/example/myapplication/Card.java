package com.example.myapplication;

import android.content.Context;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;

public class Card extends FrameLayout {
    private TextView numberView;  //呈现的文字
    private int num = 0;     //绑定的编号

    private int defaultBackColor = 0x338B8B00; // 背景色

    /**
     *
     * @param context: GameView实例的context
     */
    public Card(Context context) {
        super(context);
        numberView = new TextView(getContext());   //显示
        numberView.setTextSize(32);
        numberView.setGravity(Gravity.CENTER);
        numberView.setBackgroundColor(0x33ffffff);
        LayoutParams lp = new LayoutParams(-1, -1);  //创建个布局，填充满整个父局容器
        lp.setMargins(15, 15, 0, 0);
        addView(numberView, lp);   //然后扔进去
        setNum(0);
    }

    /**
     * 设置显示的数字并更改背景颜色
     * @param num
     */
    public void setNum(int num) {
        this.num = num;

        numberView.setBackgroundColor(getBackground(num));

        if (this.num <= 0) {
            numberView.setText("");
        } else {
            numberView.setText(num + "");
        }
    }

    public int getNum() {
        return num;
    }

    /**
     * 根据数字返回背景色
     *
     * @param num
     * @return ARGB格式的颜色十六进制值
     */
    private int getBackground(int num) {
        switch (num) {
            case 0:
                return 0xffCCC0B3;
            case 2:
                return 0xffEEE4DA;
            case 4:
                return 0xffEDE0C8;
            case 8:
                return 0xffF2B179;
            case 16:
                return 0xffF49563;
            case 32:
                return 0xffF5794D;
            case 64:
                return 0xffF55D37;
            case 128:
                return 0xffEEE863;
            case 256:
                return 0xffEDB04D;
            case 512:
                return 0xffECB04D;
            case 1024:
                return 0xffEB9437;
//            case 2048:
//                return 0xffEA7821;
            default:
                return 0xffEA7821;
        }
    }

    /**
     * 判断卡片的数字是否相同
     *
     * @param o: 比较的对象
     * @return
     */
    public boolean equals(Card o) {
        return num == o.getNum();
    }
}
