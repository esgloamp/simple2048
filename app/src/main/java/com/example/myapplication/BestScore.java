package com.example.myapplication;

import android.content.Context;
import android.content.SharedPreferences;


public class BestScore {
    private SharedPreferences s;
    public BestScore(Context context){
        s = context.getSharedPreferences("bestScore",context.MODE_PRIVATE);
    }

    public int getBestScore(){
        return s.getInt("bestScore",0);
    }
    public void setBestScore(int bestScore){
        SharedPreferences.Editor editor = s.edit();
        editor.putInt("bestScore",bestScore);
        editor.commit();
    }
}